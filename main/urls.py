from django.urls import path
from django.conf.urls import include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('galeri/', views.galeri, name='galeri'),
    path('matkul/',include('story5kybe.urls')),
]
