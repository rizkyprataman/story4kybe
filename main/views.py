from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def galeri(request):
    return render(request, 'main/galeri.html')