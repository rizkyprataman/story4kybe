from django import forms

class MatkulForm(forms.Form):

    NamaMatkul = forms.CharField(label="Nama Matkul", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    NamaDosen = forms.CharField(label="Nama Dosen", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    JumlahSKS = forms.IntegerField(label="Jumlah SKS",widget=forms.NumberInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Mata Kuliah',
        'type' : 'number',
        'required': True,
    }))
    
    DeskripsiMatkul = forms.CharField(label="Deskripsi Matkul", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    TahunSemester = forms.CharField(label="Tahun Semester", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Semester',
        'type' : 'text',
        'required': True,
    }))

    RuangKelas = forms.CharField(label="Ruang Kelas", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Ruang Kelas',
        'type' : 'text',
        'required': True,
    }))