from django.db import models

class MataKuliah(models.Model):
    NamaMatkul = models.TextField(max_length=50)
    NamaDosen = models.TextField(max_length=50)
    JumlahSKS = models.IntegerField()
    DeskripsiMatkul = models.TextField(max_length=75)
    TahunSemester = models.TextField(max_length=50)
    RuangKelas = models.TextField(max_length=50)
