from django.urls import path, re_path
from .views import buat, hapus, lihatmatkul

appname = 'Story5kybe'

urlpatterns = [
   path('', buat, name = 'buat'),
   path('<int:pk>/hapus/', hapus),
   path('<int:pk>/', lihatmatkul, name= 'xxxx'),
]