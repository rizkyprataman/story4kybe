from django.shortcuts import render,redirect
from .models import MataKuliah
from .forms import MatkulForm

# Create your views here.
def buat(request):
    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            matkul = MataKuliah()
            matkul.NamaMatkul = form.cleaned_data['NamaMatkul']
            matkul.NamaDosen = form.cleaned_data['NamaDosen']
            matkul.JumlahSKS = form.cleaned_data['JumlahSKS']
            matkul.DeskripsiMatkul = form.cleaned_data['DeskripsiMatkul']
            matkul.TahunSemester = form.cleaned_data['TahunSemester']
            matkul.RuangKelas = form.cleaned_data['RuangKelas']
            matkul.save()
        return redirect('/matkul')
    else:
        data = MataKuliah.objects.all()
        form = MatkulForm()
        response = {"matkul":data, 'form' : form}
        return render(request,'Matkul.html',response)

def hapus(request,pk):
        MataKuliah.objects.get(pk = pk).delete()
        return redirect('/matkul')

def lihatmatkul(request,pk):
        data = MataKuliah.objects.get(pk = pk)
        response = {"matkul":data}
        return render(request, 'isimatkul.html', response)
